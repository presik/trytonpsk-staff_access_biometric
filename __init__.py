# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .employee import Employee

def register():
    Pool.register(
        Employee,
        module='staff_access_biometric', type_='model')
