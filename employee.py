import base64
import pickle
import requests
from PIL import Image
import io

from trytond.exceptions import UserError
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.config import config

STATE_FACE = {
    'invisible': Eval('access_type') != 'face_access',
}
API_URL = config.get('apiface', 'api_url')
API_TOKEN = config.get('apiface', 'api_token')

class Employee(metaclass=PoolMeta):
    __name__ = 'company.employee'

    face_photo = fields.Binary('Face Photo', states=STATE_FACE)
    face_print = fields.Binary('Face Print', states=STATE_FACE)

    @fields.depends('face_photo', 'access_type')
    def on_change_face_photo(self):
        if self.face_photo and self.access_type == "face_access":
            self.face_photo = self.compress_image(self.face_photo)
            api_url = f'{API_URL}/process_image'
            b64_image = base64.b64encode(self.face_photo).decode('utf-8')
            data = {
                'b64_image': b64_image
            }
            headers = {
                "Authorization": f"Bearer {API_TOKEN}"
            }
            try:
                response = requests.post(api_url, json=data, headers=headers)
                response.raise_for_status()
                embedded_pkl_b64 = response.json().get('embedded')
                embedded_pkl = base64.b64decode(embedded_pkl_b64)
                self.face_print = embedded_pkl
            except requests.exceptions.RequestException as e:
                raise UserError(f"Error al llamar a la API de reconocimiento facial: {e}")
        else:
            self.face_print = None


    # def pre_validate(self):
    #         self.check_face_photo_size()


    def compress_image(self,image_binary, quality=50):
        try:
            image = Image.open(io.BytesIO(image_binary))
            output = io.BytesIO()
            image.save(output, format='JPEG', quality=quality, optimize=True)
            return output.getvalue()
        except Exception as e:
            raise UserError(f"Error al comprimir la imagen: {e}")


    # def check_face_photo_size(self):
    #         max_size = 1 * 1024 * 1024
    #         if self.face_photo and len(self.face_photo) > max_size:
    #             self.face_photo = None
    #             raise UserError(
    #                 "The uploaded photo exceeds the maximum allowed size of 1 MB. "
    #                 "Please upload a smaller image."
    #             )

    def employee_facial_recognition(self, b64_image):
        if self.face_print is not None:
            api_url = f'{API_URL}/facial_recognition'
            faceprint_pkl_b64 = base64.b64encode(self.face_print).decode('utf-8')
            data = {
                'b64_image': b64_image,
                'faceprint': faceprint_pkl_b64
            }
            headers = {
                "Authorization": f"Bearer {API_TOKEN}"
            }
            try:
                response = requests.post(api_url, json=data, headers=headers)
                response.raise_for_status()
                return response.json()
            except requests.RequestException as e:
                result ={
                    "status": "error",
                    "msg": f"Error en la solicitud al endpoint: {str(e)}",
                    'verified': False
                }
                return result
        else:
            result = {
                'status': 'error',
                'msg': 'No se ha registrado ninguna foto de rostro para el reconocimiento Facial',
                'verified': False
            }
            return result
